package com.pb.test;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.kafka.support.Acknowledgment;

import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableKafka
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = { KafkaClientEx2.class,KafkaProducer.class})})
public class KafkaClientEx1 extends AbstractConsumerSeekAware {

    public static void main(String[] args) {
        SpringApplication.run(KafkaClientEx1.class, args);
    }


    //@KafkaListener(topics = "testTopic", groupId = "def",containerFactory = "kafkaListenerContainerFactory")
    public void listenDef(ConsumerRecord<String,String> record) {
        System.out.println("DEF: Received Messasge key: " + record.key() + " value: " + record.value());
    };


    @KafkaListener(topics = "testTopic", groupId = "batch",containerFactory = "batchFactory")
    public void listenBatch(List<ConsumerRecord<String,String>> records,Acknowledgment acknowledgment) {
        for (ConsumerRecord<String,String> record : records) {
            System.out.println("BATCH: Received Messasge key: " + record.key() + " value: " + record.value() + " partition: " + record.partition());
        }
        acknowledgment.acknowledge();
        System.out.println("BATCH: ACK");
    };

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        System.out.println("Patrtitions assigement: ");
        assignments.forEach((t,o) -> {
            System.out.println(t.topic()+"-"+t.partition() + " offset: " + o);
            callback.seekToBeginning(t.topic(),t.partition());
        });

        System.out.println("Patrtitions reset: ");
    }

}

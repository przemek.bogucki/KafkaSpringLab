package com.pb.test;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.ContainerCustomizer;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Configuration
public class KafkaConsumerConfig {

    @Autowired
    private ApplicationArguments applicationArguments;


    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        if (applicationArguments.containsOption("h") && applicationArguments.getOptionValues("h").size() > 0) {
            String host = applicationArguments.getOptionValues("h").iterator().next();
            props.put(
                    ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        } else {
            props.put(
                    ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9091");
        }

        if (applicationArguments.containsOption("ssl")) {
            props.put("security.protocol", "SSL");
        }

        props.put(
                ConsumerConfig.GROUP_ID_CONFIG, "testGroup");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "50");

        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    public String getTopicName() {
        if (applicationArguments.containsOption("t")  && applicationArguments.getOptionValues("t").size() > 0) {
            return applicationArguments.getOptionValues("t").iterator().next();
        } else {
            return "testTopic";
        }
    }

    private ConsumerRebalanceListener consumerRebalanceListener = new ConsumerRebalanceListener() {
        @Override
        public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
            System.out.println("New partition assigned!");
        }

        @Override
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
            System.out.println("New partition assigned!");
        }
    };

    @Bean(name = "singleKafkaListener")
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> ConcurrentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.getContainerProperties().setConsumerRebalanceListener(consumerRebalanceListener);

        return factory;
    }

    @Bean(name = "batchKafkaListener")
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> batchFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConcurrency(3);
        factory.setConsumerFactory(consumerFactory());
        factory.setBatchListener(true);
        factory.getContainerProperties().setSubBatchPerPartition(true);
        factory.getContainerProperties().setIdleBetweenPolls(1000);
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL);
        factory.getContainerProperties().setAckOnError(false);
        factory.getContainerProperties().setConsumerRebalanceListener(consumerRebalanceListener);

        return factory;
    }

}

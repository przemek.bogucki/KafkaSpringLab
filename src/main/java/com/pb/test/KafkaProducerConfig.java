package com.pb.test;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.ProducerListener;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Autowired
    private ApplicationArguments applicationArguments;

    @Autowired
    private ProducerListener producerListener;

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        return new DefaultKafkaProducerFactory<String, String>(producerConfigs());
    }

    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();

        if (applicationArguments.containsOption("h") && applicationArguments.getOptionValues("h").size() > 0) {
            String host = applicationArguments.getOptionValues("h").iterator().next();
            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
        } else {
            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
        }

        if (applicationArguments.containsOption("ssl")){
            props.put("security.protocol", "SSL");

        }

        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }

    public String getTopicName() {
        if (applicationArguments.containsOption("t") && applicationArguments.getOptionValues("t").size() > 0) {
            return applicationArguments.getOptionValues("t").iterator().next();
        } else {
            return "testTopic";
        }
    }

    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        KafkaTemplate<String,String> kafkaTemplate = new KafkaTemplate<String, String>(producerFactory());
        if (producerListener != null){
            kafkaTemplate.setProducerListener(producerListener);
        }
        return kafkaTemplate;
    }

}

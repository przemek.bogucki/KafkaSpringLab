package com.pb.test;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.support.ProducerListener;

@SpringBootApplication
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = { KafkaClientEx2.class,KafkaClientEx1.class})})
public class KafkaProducer {

    public static void main(String[] args){
        SpringApplication.run(KafkaProducer.class, args);
    }

    @Autowired
    private KafkaProducerConfig producerConfig;

    @Bean
    public ApplicationRunner runner(KafkaTemplate<String, String> template) {
        return args -> {
            for (int i = 0; i < 1000; i++){
                template.send(producerConfig.getTopicName(),Integer.toString(i % 10),Integer.toString(i));
            }
        };
    }

    @Bean
    public ProducerListener<String, String> getProducerListener(){
        return new ProducerListener<String, String>() {
            @Override
            public void onSuccess(ProducerRecord<String, String> producerRecord, RecordMetadata recordMetadata) {
                System.out.println("Sucessfully sent: " + toStringRecordMetadata(producerRecord,recordMetadata));

            }

            @Override
            public void onError(ProducerRecord<String, String> producerRecord, Exception exception) {
                System.out.println("Error:" +exception+ "for record:" + producerRecord);
            }
        };
    }

    public static String toStringRecordMetadata(ProducerRecord producerRecord, RecordMetadata recordMetadata){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Key:" + producerRecord.key());
        stringBuilder.append(" value:" + producerRecord.value());

        stringBuilder.append(" <= offset:" + recordMetadata.offset());
        stringBuilder.append(" partition:" + recordMetadata.partition());
        stringBuilder.append(" topic:" + recordMetadata.topic());

        return stringBuilder.toString();

    }


}

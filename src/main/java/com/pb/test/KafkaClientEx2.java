package com.pb.test;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.listener.*;
import org.springframework.kafka.support.Acknowledgment;

import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableKafka
@ComponentScan(excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = { KafkaClientEx1.class,KafkaProducer.class})})
public class KafkaClientEx2 extends AbstractConsumerSeekAware {

    @Autowired
    private KafkaConsumerConfig kafkaConsumerConfig;

    public static void main(String[] args) {
        SpringApplication.run(KafkaClientEx2.class, args);
    }


    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekAware.ConsumerSeekCallback callback) {
        System.out.println("New patrtitions assigement: ");
        assignments.forEach((t,o) -> {
            System.out.println(t.topic()+"-"+t.partition() + " offset: " + o);
            callback.seekToBeginning(t.topic(),t.partition());
        });
    }


    @Bean
    public ApplicationRunner runner(@Qualifier("batchKafkaListener") KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> factory) {
        return args -> {
            ConcurrentMessageListenerContainer container = factory.createContainer(kafkaConsumerConfig.getTopicName());
            container.getContainerProperties().setMessageListener(new BatchAcknowledgingConsumerAwareMessageListener<String,String>() {

                @Override
                public void onMessage(List<ConsumerRecord<String, String>> records, Acknowledgment acknowledgment, Consumer<?, ?> consumer) {
                    for (ConsumerRecord<String,String> record : records) {
                        System.out.println("BATCH: Received Messasge key: " + record.key() + " value: " + record.value() + " partition: " + record.partition());
                    }
                    acknowledgment.acknowledge();
                    System.out.println("BATCH: ACK");
                }
            });
            container.start();
        };
    }


}
